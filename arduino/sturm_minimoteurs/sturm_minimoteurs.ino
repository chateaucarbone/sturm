
int value;
int del = 100;
int pins[] = {2,3,4,5,6};

 
void setup() {
  for (int i = 0; i < 5; i++) {
    pinMode(pins[i], OUTPUT);
  }
}

void loop() {
  del = random(10,200);
  
  for(value = 0 ; value <= 255; value+=1)
  {
    for (int i = 0; i < 5; i++) {
      analogWrite(pins[i], value);
    }    
    delay(del);
  }

  delay(random(15000, 40000));

  for (int i = 0; i < 5; i++) {
    analogWrite(pins[i], 0);
    }  

  delay(random(15000, 40000));
  
}
